import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent {
		
	public data:any={};
 	public cargado = false;

	public productos=[];
	public categorias=[];
	public carrito=[];
	public productosStorage=[];
	public cantidad;
	public precio;
	public elementosCarritoStorage;

	public filtroCategoria=0;
	public filtroDisponible=0;
	public filtroAgotado=0;
	public filtroMasVendidos=0;
	public filtroPrecioMayorA30=0;
	public filtroPrecioMenorA10=0;
	public nombreProducto="";
	public filtro=[];

	public ordenarnombre=0;
	public ordenarmayor=0;
	public ordenarmenor=0;
	closeResult: string;

	
	constructor(private modalService: NgbModal, private http:HttpClient) {


	http.get("../../../../assets/productos.json")
	.subscribe(res=>{
		this.data=res;
		console.log(this.data)
		this.cargado=true;
		//se inicializan los productos y categorias
		this.categorias= this.data.categories;
		this.productos = this.data.products;

		

		this.obtenerLocalStorage();

		

		let comprobarStorageCarrito= JSON.parse(localStorage.getItem("carrito"));
		if(comprobarStorageCarrito == null){
			this.cantidad = 0;
			this.precio = 0;
			
		}else{
			this.cantidad = JSON.parse(localStorage.getItem("carrito")).length;
			
		}

		


		//comprueba si el array de productos esta guardado en el local storage
		let comprobarStorageProductos = JSON.parse(localStorage.getItem("productoss"));

		if(comprobarStorageProductos == null){
			localStorage.setItem("productoss", JSON.stringify(this.productos));


		}
		
		this.productosStorage = JSON.parse(localStorage.getItem("productoss"));
	});


	}


	//Esta funcion elimina el producto al agregarlo al carrito
	eliminar(p){
		this.agregar(p);
		//this.productos.splice(i,1);
		//this.actualizarInventariosAgregarCarrito(i);
		this.elementosCarrito();
		this.obtenerLocalStorage();


	}

	//Se encarga de eliminar los productos que estan agregados en el carrito
	eliminarProductoCarrito(i){
		let carrito= this.obtenerLocalStorage();
		//Se actulizan los inventarios
		this.actualizarInventariosEliminarCarrito(i);
		carrito.splice(i,1);
		this.cantidad= carrito.length;
		//Se guarda el nuevo arreglo en el local Storage
		localStorage.setItem("carrito", JSON.stringify(carrito));
		//Se realiza la sumatoria del precio de los productos
		this.sumar(0);
		
	}

	//obtiene los productos del carrito desde Local Storage
	elementosCarrito(){
		this.cantidad = JSON.parse(localStorage.getItem("carrito")).length;
	}

	//Añade los productos al carrito
	agregar(p){
		let agregar = p
		let comprobarStorageCarrito= JSON.parse(localStorage.getItem("carrito"));
		if(comprobarStorageCarrito == null){
			this.carrito.push(agregar);
			this.guardarLocalStorage();
		}else{
			this.carrito = JSON.parse(localStorage.getItem("carrito"));
			this.carrito.push(agregar);
			this.guardarLocalStorage();
		}
		
		this.sumar(1);
	}
	
	//Calcula la suma de los precios de los productos en el carrito
	sumar(valor){
		let cantidadProductosCarrito = this.cantidad+valor;
		let carrito= this.obtenerLocalStorage();
		let suma = 0;
		for (var i = 0; i < cantidadProductosCarrito; i++){		
				suma += parseInt(carrito[i].price) * 1000; 
			}
		this.precio = suma;

	}

	//guarda los productos del carrito en el local Storage
	guardarLocalStorage(){
		localStorage.setItem("carrito", JSON.stringify(this.carrito));
	}

	//Obtiene los elementos del carrito desde el Local Storage
	obtenerLocalStorage(){
		this.elementosCarritoStorage = JSON.parse(localStorage.getItem("carrito"));
		return this.elementosCarritoStorage;
		
}

	
	/*
	 Actualiza los inventarios al añadir los productos al carrito, recibe como parametro el indice
	 del arreglo de objetos de los productos que esta almacenado en el Local Storage
	*/
	actualizarInventariosAgregarCarrito(i){
		let inputId= i+1;
		localStorage.setItem("productoss", JSON.stringify(this.productosStorage));
	}


	/*
	 Actualiza los inventarios al añadir los productos al carrito, recibe como parametro el indice
	 del arreglo de objetos del carrito de compras que esta almacenado en el Local Storage
	*/

	actualizarInventariosEliminarCarrito(i){
		let inputId= this.elementosCarritoStorage[i].id;
		
		localStorage.setItem("productoss", JSON.stringify(this.productosStorage));
	}


	//Funcion Para la venta Modal, para realizarla se utilizo https://ng-bootstrap.github.io
	open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.sumar(0);
  }

//Funcion Para la ventana Modal, para realizarla se utilizo 
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }


  }

  ordenarPorNombre(arr :any){
	return arr.sort((a,b)=>{
		if((a.name.toLowerCase()) > b.name.toLowerCase()){
			return 1;
		}
		if(a.name.toLowerCase() < b.name.toLowerCase()){
			return -1;
		}
		return 0;
	});
  }

  ordenarPrecioMayor(arr :any){
	return arr.sort((a,b)=>{
		if(parseInt(a.price) < parseInt(b.price)){
			return 1;
		}
		if(parseInt(a.price) > parseInt(b.price)){
			return -1;
		}
		return 0;
	});
  }

  ordenarPrecioMenor(arr :any){
	return arr.sort((a,b)=>{
		if(parseInt(a.price) > parseInt(b.price)){
			return 1;
		}
		if(parseInt(a.price) < parseInt(b.price)){
			return -1;
		}
		return 0;
	});
  }

    ordenarNombre(){
		this.ordenarnombre=1;
		this.ordenarmayor=0;
		this.ordenarmenor=0;
	}
	ordenarMayor(){
		this.ordenarnombre=0;
		this.ordenarmayor=1;
		this.ordenarmenor=0;
	}
	ordenarMenor(){
		this.ordenarnombre=0;
		this.ordenarmayor=0;
		this.ordenarmenor=1;
	}

	eliminarOrden(){
		this.ordenarnombre=0;
		this.ordenarmayor=0;
		this.ordenarmenor=0;
	}


  //Funcion Para realizar los filtros de un producto

  aplicarFiltro(producto: any){
    var cat=false;
    var res=false;
    let ret=false;
    
    if(this.filtroDisponible && producto.available == true){
      res=true;
    }
    else if(this.filtroAgotado && producto.available == false){
      res=true;
    }
    else if(this.filtroMasVendidos && producto.best_seller == true){
      res=true;
    }
    else if(this.filtroPrecioMayorA30 && parseFloat(producto.price)>30.000){
      res=true;
    }
    else if(this.filtroPrecioMenorA10 && parseFloat(producto.price)<10.000){
      res=true;
    }
    else {
      res=false;
    }

    if(this.filtroAgotado || this.filtroDisponible || this.filtroMasVendidos || this.filtroPrecioMayorA30 || this.filtroPrecioMenorA10){
      ret=true;
    }

    if(this.filtroCategoria != 0 && producto.categories.includes(this.filtroCategoria)){
      cat=true;
    }
    else{
      cat=false;
    }

    if(this.nombreProducto != "" && producto.name.toLowerCase().includes(this.nombreProducto.toLowerCase())){
	this.filtroCategoria=0;
	this.filtroDisponible=0;
	this.filtroAgotado=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMayorA30=0;
	this.filtroPrecioMenorA10=0;
      return true

    }

    if(this.filtroCategoria == 0 && res){
      return true;
    }


    if(this.filtroCategoria != 0 && (cat && res)){
      return true;
    }
    if(this.filtroCategoria != 0 && (!cat && res)){

      return false;
    }
    if(this.filtroCategoria != 0 && (cat && !res)){
      return true;
    }

    
    return false;
  }

  //Funcion para aplicar filtro a todos los productos
  filtrar(){
	  if(this.filtrando()){
		let f;
		this.filtro=[]
		for(let p of this.productosStorage){
			let res = this.aplicarFiltro(p);
			if(res){
			  this.filtro.push(p);
			}
			
		}
		f = this.filtro

		if(this.ordenarmenor){
			
			return this.ordenarPrecioMenor(f);
		}
		if(this.ordenarmayor){
			
			return this.ordenarPrecioMayor(f)
		}
		if(this.ordenarnombre){
			return this.ordenarPorNombre(f)
		}
		return this.filtro
	  }
	  else{
		let p;
		p= this.productosStorage
		if(this.ordenarmenor){
			return this.ordenarPrecioMenor(p)
		}
		if(this.ordenarmayor){
			return this.ordenarPrecioMayor(p)
		}
		if(this.ordenarnombre){
			return this.ordenarPorNombre(p)
		}
		return this.productosStorage;
	  }
	  
  }

  filtrando(){
	if(this.filtroAgotado || this.filtroDisponible || this.filtroMasVendidos || this.filtroPrecioMayorA30 || this.filtroPrecioMenorA10 || this.nombreProducto != "" || this.filtroCategoria != 0 ){
		return true;
	  }
	else{
		return false
	}
  }

  filtrarDisponibles(){
	  this.filtroDisponible=1;
	  this.filtroCategoria=0;
	this.filtroAgotado=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMayorA30=0;
	this.filtroPrecioMenorA10=0;
  }
  filtrarAgotados(){
	  this.filtroAgotado=1;
	  this.filtroCategoria=0;
	this.filtroDisponible=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMayorA30=0;
	this.filtroPrecioMenorA10=0;
  }
  filtrarMasVendidos(){
	  this.filtroMasVendidos=1;
	  this.filtroCategoria=0;
	this.filtroDisponible=0;
	this.filtroAgotado=0;
	this.filtroPrecioMayorA30=0;
	this.filtroPrecioMenorA10=0;
  }
  filtrarMayor(){
	  this.filtroPrecioMayorA30=1;
	  this.filtroCategoria=0;
	this.filtroDisponible=0;
	this.filtroAgotado=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMenorA10=0;
  }
  filtrarMenor(){
	this.filtroPrecioMenorA10=1;
	this.filtroCategoria=0;
	this.filtroDisponible=0;
	this.filtroAgotado=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMayorA30=0;
  }
  filtrarCategoria(cat:any){
	this.filtroCategoria= cat.categori_id;
	this.filtroDisponible=0;
	this.filtroAgotado=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMayorA30=0;
	this.filtroPrecioMenorA10=0;
  }
  eliminarFiltros(){
	this.filtroCategoria=0;
	this.filtroDisponible=0;
	this.filtroAgotado=0;
	this.filtroMasVendidos=0;
	this.filtroPrecioMayorA30=0;
	this.filtroPrecioMenorA10=0;
  }

  


}
