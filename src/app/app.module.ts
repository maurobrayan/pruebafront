import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, NgModel} from '@angular/forms'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from "@angular/common/http";


import { AppComponent } from './app.component';
import { ProductosComponent } from './componentes/productos/productos.component';
import { CarritoComponent } from './componentes/carrito/carrito.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    CarritoComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
